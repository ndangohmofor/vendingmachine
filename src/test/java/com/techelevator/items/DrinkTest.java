package com.techelevator.items;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class DrinkTest {

    Drink myDrink = new Drink("Cola", BigDecimal.valueOf(1.25), "C1");

    @Test
    public void getDispenseMessage() {
        assertEquals("Glug Glug, Yum!", myDrink.getDispenseMessage());
    }
}