package com.techelevator.items;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ChipsTest {

    Chips myChip = new Chips("Potato Crisps", BigDecimal.valueOf(3.05), "A1");

    @Test
    public void getDispenseMessage() {
        assertEquals("Crunch Crunch, Yum!", myChip.getDispenseMessage());
    }
}