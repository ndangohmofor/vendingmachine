package com.techelevator.items;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class GumTest {

    Gum myGum = new Gum("Chiclets", BigDecimal.valueOf(0.75), "D3");

    @Test
    public void getDispenseMessage() {
        assertEquals("Chew Chew, Yum!", myGum.getDispenseMessage());
    }
}