package com.techelevator.items;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CandyTest {

    Candy myCandy = new Candy("Moonpie", BigDecimal.valueOf(1.80), "B1");

    @Test
    public void getDispenseMessage() {
        assertEquals("Munch Munch, Yum!", myCandy.getDispenseMessage());
    }


}