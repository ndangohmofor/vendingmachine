package com.techelevator.menus;

import com.techelevator.items.VendingMachineItems;
import com.techelevator.utilities.VendingMachineUtils;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DisplayMenuTest {

    private final List<VendingMachineItems> vendingMachineStock = VendingMachineUtils.vendStock();

    @Test
    public void getVendingMachineItemsAvailableList() {
        String expected = "Slot: A1\tPrice: $3.05 \tQuantity: 5\tName: Potato Crisps\n" +
                "Slot: A2\tPrice: $1.45 \tQuantity: 5\tName: Stackers\n" +
                "Slot: A3\tPrice: $2.75 \tQuantity: 5\tName: Grain Waves\n" +
                "Slot: A4\tPrice: $3.65 \tQuantity: 5\tName: Cloud Popcorn\n" +
                "Slot: B1\tPrice: $1.80 \tQuantity: 5\tName: Moonpie\n" +
                "Slot: B2\tPrice: $1.50 \tQuantity: 5\tName: Cowtales\n" +
                "Slot: B3\tPrice: $1.50 \tQuantity: 5\tName: Wonka Bar\n" +
                "Slot: B4\tPrice: $1.75 \tQuantity: 5\tName: Crunchie\n" +
                "Slot: C1\tPrice: $1.25 \tQuantity: 5\tName: Cola\n" +
                "Slot: C2\tPrice: $1.50 \tQuantity: 5\tName: Dr. Salt\n" +
                "Slot: C3\tPrice: $1.50 \tQuantity: 5\tName: Mountain Melter\n" +
                "Slot: C4\tPrice: $1.50 \tQuantity: 5\tName: Heavy\n" +
                "Slot: D1\tPrice: $0.85 \tQuantity: 5\tName: U-Chews\n" +
                "Slot: D2\tPrice: $0.95 \tQuantity: 5\tName: Little League Chew\n" +
                "Slot: D3\tPrice: $0.75 \tQuantity: 5\tName: Chiclets\n" +
                "Slot: D4\tPrice: $0.75 \tQuantity: 5\tName: Triplemint\n";

        String actual = "";
        for (VendingMachineItems item : vendingMachineStock) {
            actual += item.toString().trim() + "\n";
        }

        assertEquals(expected, actual);
    }

    @Test
    public void getVendingMachineItemsOutOfStockList() {
        String expected = "Slot: A1\tPrice: $3.05 \tQuantity: 5\tName: Potato Crisps\n" +
                "Slot: A2\tPrice: $1.45 \tQuantity: 5\tName: Stackers\n" +
                "Slot: A3\tPrice: $2.75 \tQuantity: 5\tName: Grain Waves\n" +
                "Slot: A4\tPrice: $3.65 \tQuantity: 5\tName: Cloud Popcorn\n" +
                "Slot: B1\tPrice: $1.80 \tQuantity: 5\tName: Moonpie\n" +
                "Slot: B2\tPrice: $1.50 \tQuantity: 5\tName: Cowtales\n" +
                "Slot: B3\tPrice: $1.50 \tQuantity: 5\tName: Wonka Bar\n" +
                "Slot: B4\tPrice: $1.75 \tQuantity: 5\tName: Crunchie\n" +
                "Slot: C1\tPrice: $1.25 \tQuantity: 5\tName: Cola\n" +
                "Slot: C2\tPrice: $1.50 \tQuantity: 5\tName: Dr. Salt\n" +
                "Slot: C3\tPrice: $1.50 \tQuantity: 5\tName: Mountain Melter\n" +
                "Slot: C4\tPrice: $1.50 \tQuantity: 5\tName: Heavy\n" +
                "Slot: D1\tPrice: $0.85 \tQuantity: 5\tName: U-Chews\n" +
                "Slot: D2\tPrice: $0.95 \tQuantity: 5\tName: Little League Chew\n" +
                "Slot: D3\tPrice: $0.75 \tQuantity: 5\tName: Chiclets\n" +
                "Slot: D4\tPrice: $0.75 \tQuantity: SOLD OUT\tName: Triplemint\n";

        String actual = "";
        vendingMachineStock.get(15).setQuantity(0);
        for (VendingMachineItems item : vendingMachineStock) {
            actual += item.toString().trim() + "\n";
        }

        assertEquals(expected, actual);
    }

    @Test
    public void getVendingMachineItemsUsedItemList() {
        String expected = "Slot: A1\tPrice: $3.05 \tQuantity: 5\tName: Potato Crisps\n" +
                "Slot: A2\tPrice: $1.45 \tQuantity: 5\tName: Stackers\n" +
                "Slot: A3\tPrice: $2.75 \tQuantity: 5\tName: Grain Waves\n" +
                "Slot: A4\tPrice: $3.65 \tQuantity: 5\tName: Cloud Popcorn\n" +
                "Slot: B1\tPrice: $1.80 \tQuantity: 5\tName: Moonpie\n" +
                "Slot: B2\tPrice: $1.50 \tQuantity: 5\tName: Cowtales\n" +
                "Slot: B3\tPrice: $1.50 \tQuantity: 5\tName: Wonka Bar\n" +
                "Slot: B4\tPrice: $1.75 \tQuantity: 5\tName: Crunchie\n" +
                "Slot: C1\tPrice: $1.25 \tQuantity: 5\tName: Cola\n" +
                "Slot: C2\tPrice: $1.50 \tQuantity: 5\tName: Dr. Salt\n" +
                "Slot: C3\tPrice: $1.50 \tQuantity: 3\tName: Mountain Melter\n" +
                "Slot: C4\tPrice: $1.50 \tQuantity: 5\tName: Heavy\n" +
                "Slot: D1\tPrice: $0.85 \tQuantity: 5\tName: U-Chews\n" +
                "Slot: D2\tPrice: $0.95 \tQuantity: 5\tName: Little League Chew\n" +
                "Slot: D3\tPrice: $0.75 \tQuantity: 5\tName: Chiclets\n" +
                "Slot: D4\tPrice: $0.75 \tQuantity: 5\tName: Triplemint\n";

        String actual = "";
        vendingMachineStock.get(10).setQuantity(3);
        for (VendingMachineItems item : vendingMachineStock) {
            actual += item.toString().trim() + "\n";
        }

        assertEquals(expected, actual);
    }
}