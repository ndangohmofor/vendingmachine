package com.techelevator.utilities;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

public class FeedMoneyUtilTest {

    @Test
    public void feedMoney() {

        assertEquals(BigDecimal.valueOf(15.00).setScale(2, RoundingMode.DOWN), FeedMoneyUtil.feedMoney(BigDecimal.valueOf(10.00), BigDecimal.valueOf(5.00)));
    }
}