package com.techelevator.logging;

import com.techelevator.items.Candy;
import com.techelevator.money.MoneyManager;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Scanner;

import static org.junit.Assert.*;

public class LoggerTest {

    Candy myCandy = new Candy("Moonpie", BigDecimal.valueOf(1.80), "B1");
    MoneyManager myMoney = new MoneyManager(BigDecimal.valueOf(10.00), BigDecimal.valueOf(5.00));
    File logFile = new File("Log.txt");

    @Test
    public void loggerFileExists() {
        Logger.logger(myCandy.getDispenseMessage(), myMoney.getAvailableBalance(), myCandy.getPrice());
        boolean exists = logFile.exists();
        assertTrue(exists);
    }

    @Test
    public void loggerFileIsNotEmpty(){
        boolean isEmpty = false;
        try {
            Scanner fileScanner = new Scanner(logFile);
            isEmpty = fileScanner.hasNextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        assertTrue(isEmpty);
    }

}