package com.techelevator.money;

import java.math.BigDecimal;

public class MoneyManager {
    private BigDecimal currentMoneyProvided;
    private BigDecimal availableBalance;

    public MoneyManager(BigDecimal currentMoneyProvided, BigDecimal availableBalance){
        this.currentMoneyProvided = currentMoneyProvided;
        this.availableBalance = availableBalance;
    }

    public BigDecimal getCurrentMoneyProvided() {
        return currentMoneyProvided;
    }

    public void setCurrentMoneyProvided(BigDecimal currentMoneyProvided) {
        this.currentMoneyProvided = currentMoneyProvided;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }
}
