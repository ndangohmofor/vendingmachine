package com.techelevator.menus;

import com.techelevator.items.VendingMachineItems;

import java.math.RoundingMode;
import java.util.List;

public class DisplayMenu {
    private static List<VendingMachineItems> vendingMachineItemsList;

    public DisplayMenu(List<VendingMachineItems> vendingMachineItemsList) {
        this.vendingMachineItemsList = vendingMachineItemsList;
    }

    public static void getVendingMachineItemsList(List<VendingMachineItems> vendingMachineItemsList) {
        for(VendingMachineItems item: vendingMachineItemsList){
            if(item.getQuantity() == 0){
                System.out.printf("Slot: %s \tPrice: %s \tQuantity: SOLD OUT \tName: %s\n", item.getSlot(), item.getPrice().setScale(2, RoundingMode.DOWN), item.getName());
            } else {
                System.out.printf("Slot: %s \tPrice: $%s \tQuantity: %s \tName: %s\n", item.getSlot(), item.getPrice().setScale(2, RoundingMode.DOWN), item.getQuantity(), item.getName());
            }
        }
    }
}
