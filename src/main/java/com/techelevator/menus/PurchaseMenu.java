package com.techelevator.menus;

public class PurchaseMenu {

    //Purchase menu options
    private static final String PURCHASE_MENU_OPTION_FEED_MONEY = "(1) Feed Money";
    private static final String PURCHASE_MENU_OPTION_SELECT_PRODUCT = "(2) Select Product";
    private static final String FINISH_TRANSACTION = "(3) Finish Transaction";
    private static String[] PURCHASE_MENU_OPTIONS = {PURCHASE_MENU_OPTION_FEED_MONEY, PURCHASE_MENU_OPTION_SELECT_PRODUCT, FINISH_TRANSACTION};

    public PurchaseMenu() {
    }

    public static void getPurchaseMenuOptions() {
        System.out.println("Please choose from the following options (1, 2, or 3): ");
        for(String menu: PURCHASE_MENU_OPTIONS){
            System.out.println(menu);
        }
    }
}
