package com.techelevator.menus;

import java.util.ArrayList;
import java.util.List;

public class MainMenu {

    //Main Menu options
    private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "(1) Display Vending Machine Items";
    private static final String MAIN_MENU_OPTION_PURCHASE = "(2) Purchase";
    private static final String mainMenuOptionExit = "(3) Exit";
    private static String[] MAIN_MENU_OPTIONS = {MAIN_MENU_OPTION_DISPLAY_ITEMS, MAIN_MENU_OPTION_PURCHASE, mainMenuOptionExit};

    public MainMenu() {
    }

    public static void getMainMenuOptions() {
        for(String menu: MAIN_MENU_OPTIONS){
            System.out.println(menu);
        }
    }
}
