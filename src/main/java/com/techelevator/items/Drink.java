package com.techelevator.items;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Drink extends VendingMachineItems{
    private String dispenseMessage = "Glug Glug, Yum!";

    public Drink(String name, BigDecimal price, String slot) {
        super(name, price, slot);
        this.dispenseMessage = getDispenseMessage();
    }


    @Override
    public String getDispenseMessage() {
        return dispenseMessage;
    }

    @Override
    public String toString() {
        if (getQuantity() > 0) {
            return "Slot: " + getSlot() + "\tPrice: $" + getPrice().setScale(2, RoundingMode.DOWN) + " \tQuantity: " + getQuantity() + "\tName: " + getName();
        } else {
            return "Slot: " + getSlot() + "\tPrice: $" + getPrice().setScale(2, RoundingMode.DOWN) + " \tQuantity: " + "SOLD OUT" + "\tName: " + getName();
        }
    }
}
