package com.techelevator.items;

import java.math.BigDecimal;


public abstract class VendingMachineItems {
    private String name;
    private BigDecimal price;
    private String slot;
    private int quantity = 5;

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getSlot() {
        return slot;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public VendingMachineItems(String name, BigDecimal price, String slot) {
        this.name = name;
        this.price = price;
        this.slot = slot;
        this.quantity = 5;
    }

    public abstract String getDispenseMessage();

    public abstract String toString();
}