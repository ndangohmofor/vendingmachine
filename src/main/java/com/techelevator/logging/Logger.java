package com.techelevator.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    private static LocalDateTime entryDateTime = LocalDateTime.now();
    private static DateTimeFormatter entryFormatDateTime = DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm:ss a");
    private static File logFile = new File("Log.txt");
//    private static String message;
    //private static ;

    public static void logger(String message, BigDecimal change, BigDecimal balance) {
        try(PrintWriter logWrite = new PrintWriter(new FileOutputStream(logFile, true))){
            logWrite.printf("%s %s %s %s %s\n", ">", entryDateTime.format(entryFormatDateTime), message, change, balance);
        }catch (FileNotFoundException e){
            System.out.println("The log file was not found!");
            System.exit(1);
        }
    }
}
