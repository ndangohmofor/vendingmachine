package com.techelevator.utilities;

import com.techelevator.logging.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class FeedMoneyUtil {

    private BigDecimal availableBalance;
    private BigDecimal moneyProvided;

    /*public static FeedMoneyUtil() {
    }*/

    public static BigDecimal feedMoney(BigDecimal moneyProvided, BigDecimal availableBalance){
        availableBalance = availableBalance.add(moneyProvided).setScale(2, RoundingMode.DOWN);
        Logger.logger("FEED MONEY: ", moneyProvided.setScale(2, RoundingMode.DOWN), availableBalance.setScale(2, RoundingMode.DOWN));
        return availableBalance;
    }
}
