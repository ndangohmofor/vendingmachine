package com.techelevator.utilities;

import com.techelevator.items.VendingMachineItems;
import com.techelevator.logging.Logger;
import com.techelevator.money.MoneyManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class ProductPurchaseUtil {

    public ProductPurchaseUtil() {
    }

    public static void inventoryUpdate(List<VendingMachineItems> vendingMachineStock, List<String> availableStockCodes, List<String> soldOutStockCodes) {
        for (VendingMachineItems item : vendingMachineStock) {
            if (item.getQuantity() > 0) {
                availableStockCodes.add(item.getSlot());
            } else {
                availableStockCodes.remove(item.getSlot());
                soldOutStockCodes.add(item.getSlot());
            }
        }
    }

    public static void productPurchase(VendingMachineItems item, MoneyManager myMoney) {
        BigDecimal oldBalance = myMoney.getAvailableBalance().setScale(2, RoundingMode.HALF_DOWN);
        myMoney.setAvailableBalance(myMoney.getAvailableBalance().subtract(item.getPrice()));
        item.setQuantity(item.getQuantity() - 1);
        System.out.printf("%s \nPrice: $%s \nBalance: $%s\n%s\n\n\n", item.getName(), item.getPrice(), myMoney.getAvailableBalance(), item.getDispenseMessage());
        Logger.logger(item.getDispenseMessage(), oldBalance.setScale(2, RoundingMode.DOWN), myMoney.getAvailableBalance().setScale(2, RoundingMode.DOWN));
        //return myMoney.getAvailableBalance();
    }

    public static void changeDispenser(MoneyManager myMoney, int change) {
        change = myMoney.getAvailableBalance().multiply(BigDecimal.valueOf(100)).intValue();
        int numberQuarters = change / 25;
        change = change % 25;
        int numberDimes = change / 10;
        change = change % 10;
        int numberNickles = change / 5;
        change = change % 5;
        System.out.printf("You change is %s quarters, %s dimes, %s nickles, %s cents\n\n", numberQuarters, numberDimes, numberNickles, change);
        Logger.logger("GIVE CHANGE: ", myMoney.getAvailableBalance().setScale(2, RoundingMode.DOWN), BigDecimal.ZERO.setScale(2, RoundingMode.DOWN));
        myMoney.setAvailableBalance(BigDecimal.ZERO);
        System.out.println("Thank you! Come back soon!");
    }
}
