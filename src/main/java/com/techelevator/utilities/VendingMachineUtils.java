package com.techelevator.utilities;

import com.techelevator.items.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VendingMachineUtils {
    public static List<VendingMachineItems> vendStock() {
        List<VendingMachineItems> vendingMachineStock = new ArrayList<>();
        File itemFile = new File("vendingmachine.csv");

        try (Scanner fileScanner = new Scanner(itemFile)) {
            while (fileScanner.hasNextLine()) {
                String[] itemValues = fileScanner.nextLine().split("\\|");

                switch (itemValues[3]) {
                    case "Chip": {
                        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(itemValues[2]));
                        Chips myChip = new Chips(itemValues[1], price, itemValues[0]);
                        vendingMachineStock.add(myChip);
                        break;
                    }
                    case "Candy": {
                        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(itemValues[2]));
                        Candy myCandy = new Candy(itemValues[1], price, itemValues[0]);
                        vendingMachineStock.add(myCandy);
                        break;
                    }
                    case "Drink": {
                        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(itemValues[2]));
                        Drink myDrink = new Drink(itemValues[1], price, itemValues[0]);
                        vendingMachineStock.add(myDrink);
                        break;
                    }
                    case "Gum": {
                        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(itemValues[2]));
                        Gum myGum = new Gum(itemValues[1], price, itemValues[0]);
                        vendingMachineStock.add(myGum);
                        break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("The stocking file was not found");
            System.exit(1);
        }
        return vendingMachineStock;
    }
}
