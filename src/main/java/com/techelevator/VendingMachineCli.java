package com.techelevator;

import com.techelevator.items.VendingMachineItems;
import com.techelevator.menus.DisplayMenu;
import com.techelevator.menus.MainMenu;
import com.techelevator.menus.PurchaseMenu;
import com.techelevator.money.MoneyManager;
import com.techelevator.utilities.FeedMoneyUtil;
import com.techelevator.utilities.ProductPurchaseUtil;
import com.techelevator.utilities.VendingMachineUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VendingMachineCli {
    //Main Menu options
    private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "(1) Display Vending Machine Items";
    private static final String MAIN_MENU_OPTION_PURCHASE = "(2) Purchase";
    private static final String mainMenuOptionExit = "(3) Exit";
    private static final String[] MAIN_MENU_OPTIONS = {MAIN_MENU_OPTION_DISPLAY_ITEMS, MAIN_MENU_OPTION_PURCHASE, mainMenuOptionExit};

    //Purchase menu options
    private static final String PURCHASE_MENU_OPTION_FEED_MONEY = "(1) Feed Money";
    private static final String PURCHASE_MENU_OPTION_SELECT_PRODUCT = "(2) Select Product";
    private static final String FINISH_TRANSACTION = "(3) Finish Transaction";
    private static final String[] PURCHASE_MENU_OPTIONS = {PURCHASE_MENU_OPTION_FEED_MONEY, PURCHASE_MENU_OPTION_SELECT_PRODUCT, FINISH_TRANSACTION};

    private Scanner userInput = new Scanner(System.in);
    private final List<VendingMachineItems> vendingMachineStock = VendingMachineUtils.vendStock();
    private final List<String> soldOutStockCodes = new ArrayList<>();
    private final List<String> availableStockCodes = new ArrayList<>();
    private MoneyManager myMoney;

    public VendingMachineCli() {
    }

    public static void main(String[] args) {
        VendingMachineCli cli = new VendingMachineCli();

        cli.run();
    }

    public void run() {
        MoneyManager myMoney = new MoneyManager(BigDecimal.ZERO, BigDecimal.ZERO);
        MainMenu:
        while (true) {
            System.out.println("Choose from the following options (1, 2, or 3): ");

            MainMenu.getMainMenuOptions();
            String userSelection = userInput.nextLine();
            if (userSelection.equalsIgnoreCase("1")) {

                DisplayMenu displayMenu = new DisplayMenu(vendingMachineStock);
                DisplayMenu.getVendingMachineItemsList(vendingMachineStock);
                //Generate list of available stock codes

            } else if (userSelection.equalsIgnoreCase("2")) {
                PurchaseMenu:
                while (true) {
                    PurchaseMenu.getPurchaseMenuOptions();
                    System.out.printf("Current Money Provided: $%s\n", myMoney.getAvailableBalance());
                    userSelection = userInput.nextLine();
                    if (userSelection.equalsIgnoreCase("1")) {
                        //Feed money Menu
                        System.out.println("Please enter the dollar amount you want to feed in (1, 2, 5 or 10):");
                        FeedMoney:
                        while (userInput.hasNextLine()) {
                            userSelection = userInput.nextLine();
                            if (userSelection.equalsIgnoreCase("1") || userSelection.equalsIgnoreCase("2") || userSelection.equalsIgnoreCase("5") || userSelection.equalsIgnoreCase("10")) {
                                Integer userSelectionInt = Integer.parseInt(userSelection);
                                myMoney.setAvailableBalance(FeedMoneyUtil.feedMoney(BigDecimal.valueOf(userSelectionInt), myMoney.getAvailableBalance()));
                                System.out.printf("Your current balance is: $%s\n", myMoney.getAvailableBalance());
                                System.out.println("Feed more money if desired. When done, press 0:");
                            } else if (userSelection.equalsIgnoreCase("0")) {
                                continue PurchaseMenu;
                            } else {
                                System.out.println("Invalid bill provided. Please enter 1, 2, 5 or 10");
                                continue FeedMoney;
                            }
                        }
                    } else if (userSelection.equalsIgnoreCase("2")) {
                        //Enter Select product flow
                        DisplayMenu.getVendingMachineItemsList(vendingMachineStock);

                        System.out.println("Please select an item from the above list using the provided code. Enter 0 when done:");
                        while (userInput.hasNextLine()) {
                            ProductPurchaseUtil.inventoryUpdate(vendingMachineStock, availableStockCodes, soldOutStockCodes);
                            userSelection = userInput.nextLine().toUpperCase();
                            if (userSelection.equals("0")) {
                                break;
                            } else if (soldOutStockCodes.contains(userSelection)) {
                                //User's selection is in vending machine Sold out list
                                System.out.printf("The item is sold out, please select something else! Balance is $%s\n", myMoney.getAvailableBalance());
                            } else if (!availableStockCodes.contains(userSelection)) {
                                System.out.printf("The item you selected is not in the list. Please try again. Balance: $%s\n", myMoney.getAvailableBalance());
                            } else {
                                for (VendingMachineItems item : vendingMachineStock) {
                                    if (item.getSlot().equals(userSelection)) {
                                        if (myMoney.getAvailableBalance().compareTo(item.getPrice()) > 0) {
                                            ProductPurchaseUtil.productPurchase(item, myMoney);
                                        } else {
                                            System.out.printf("Your balance is not sufficient to make the purchase! \nBalance: $%s. Price: $%s\n", myMoney.getAvailableBalance(), item.getPrice());
                                            System.out.println("Select another item or press 0 to feed more money");
                                        }
                                    }
                                }
                            }
                            DisplayMenu.getVendingMachineItemsList(vendingMachineStock);
                            System.out.printf("Your available balance is $%s. Enter 0 when you are done!\n", myMoney.getAvailableBalance());
                        }
                    } else if (userSelection.equalsIgnoreCase("3")) {
                        //Provide change and return to main menu
                        ProductPurchaseUtil.changeDispenser(myMoney, 0);
                        break;
                    }
                }
            } else if (userSelection.equalsIgnoreCase("3")) {
                break;
            } else if (userSelection.equalsIgnoreCase("4")){
                //Generate a sales report
            }
        }
    }
}
